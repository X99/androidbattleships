package battleships.formation.excilys.com.battleships;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class NameInputActivity extends AppCompatActivity {

    private TextView nameTextField;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_name_input);
        setContentView(R.layout.activity_player_name);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        nameTextField = (TextView)findViewById(R.id.playerName);

        setSupportActionBar(toolbar);

        Button fab = (Button) findViewById(R.id.validatePlayerNameButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Log.i("NameInputActivity", "onClick: "+nameTextField.getText());
            }
        });
    }
}
