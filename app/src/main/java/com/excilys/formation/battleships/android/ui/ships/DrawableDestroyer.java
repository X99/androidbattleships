package com.excilys.formation.battleships.android.ui.ships;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;

import java.util.HashMap;
import java.util.Map;

import battleships.formation.excilys.com.battleships.R;
import battleships.ship.AbstractShip;
import battleships.ship.BattleShip;
import battleships.ship.Destroyer;

/**
 * Created by scaltot on 12/09/17.
 */

public class DrawableDestroyer extends Destroyer implements DrawableShip {

    //TODO insert map declaration
    static final Map<Orientation, Integer> DRAWABLES = new HashMap<Orientation, Integer>();

    //TODO add static block using myMap.put(key, value) to initialize map.
    //TODO key = the ship orientation and value = the drawable corresponding
    static {
        // le code à l'intérieur de ce bloc sera éxecuté une seule fois au chargement de la classe Example.
        // les opérations effectuées sont donc partagées par toutes les instances de Example.
        DRAWABLES.put(Orientation.NORTH, R.drawable.destroyer_n);
        DRAWABLES.put(Orientation.SOUTH, R.drawable.destroyer_s);
        DRAWABLES.put(Orientation.EAST, R.drawable.destroyer_e);
        DRAWABLES.put(Orientation.WEST, R.drawable.destroyer_w);
    }

    // TODO return the right drawable according to ship's orientation using the map
    @Override
    public int getDrawable(Orientation o) {
        return DRAWABLES.get(o);
    }
}
