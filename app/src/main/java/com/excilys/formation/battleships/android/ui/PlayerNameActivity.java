package com.excilys.formation.battleships.android.ui;

import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import battleships.formation.excilys.com.battleships.R;

public class PlayerNameActivity extends AppCompatActivity {

    private TextView nameTextField;
    private Toolbar toolbar;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_name_input);
        setContentView(R.layout.activity_player_name);

        //get app preference cache
        preferences = getApplicationContext().getSharedPreferences("Pref", MODE_PRIVATE);

        String playerName = preferences.getString("playerName", null);

        if(playerName != null) {
            BattleShipsApplication.getGame().init(playerName);
        }


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        nameTextField = (TextView)findViewById(R.id.playerName);

        Button fab = (Button) findViewById(R.id.validatePlayerNameButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();

                String playerName =  nameTextField.getText().toString();
                preferences.edit().putString("playerName", playerName).apply();
                //Log.i("NameInputActivity", "onClick: "+nameTextField.getText());
                BattleShipsApplication.getGame().init(nameTextField.getText().toString());
            }
        });
    }

}
