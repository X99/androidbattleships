package com.excilys.formation.battleships.android.ui.ships;

import battleships.ship.AbstractShip;

public interface DrawableShip {
    int getDrawable(AbstractShip.Orientation o);
}
