package com.excilys.formation.battleships.android.ui;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import battleships.formation.excilys.com.battleships.R;

public class ScoreActivity extends AppCompatActivity {


    public static class Extra {
        public static String WIN = "EXTRA_WIN";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        boolean win = getIntent().getExtras().getBoolean(Extra.WIN);
        TextView winLabel = (TextView) findViewById(R.id.score_win_label);
        TextView loseLabel = (TextView) findViewById(R.id.score_lose_label);
        ImageView imageWin = (ImageView) findViewById(R.id.imageWin);

        int winVisible = View.VISIBLE, loseVisible = View.VISIBLE;
        if (win) {
            loseVisible = View.GONE;
        } else {
            winVisible = View.GONE;
        }

        imageWin.setVisibility(winVisible);
        winLabel.setVisibility(winVisible);
        loseLabel.setVisibility(loseVisible);

    }

}
